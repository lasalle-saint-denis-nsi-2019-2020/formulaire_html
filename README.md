Un formulaire POST avec Flask
===

L'idée générale est de distribuer une application [Flask](https://flask.palletsprojects.com)
sur [Heroku](https://heroku.com).
Pour cela, on utilisera le serveur [gunicorn](https://gunicorn.org/), un serveur WGSI léger.

La ligne de commande à lancer est celle qui correspond à `web` dans
[Procfile](/Procfile) : c'est ce qu'on lancerait à la main sur notre machine.

Avant toute chose, il a été nécessaire de créer un compte sur Heroku et d'y ajouter
une application et un Pipeline d'exécution, au moins pour une branche staging.

En utilisant [la doc de giltab](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html).
Attention, celle-ci est incomplète et il faut consulter mon fichier [.gitlab-ci.yml](/.gitlab-ci.yml).

La référence la plus intéressante sur le sujet est ce 
[billet de blog](https://blog.ruanbekker.com/blog/2019/01/05/tutorial-on-using-gitlab-cicd-pipelines-to-deploy-your-python-flask-restful-api-with-postgres-on-heroku/).

Pour surveiller l'évolution des builds : https://dashboard.heroku.com/pipelines/0ed0d725-69ef-472a-ac89-25b3ca46bf05

Quelques références sur ce projet :
  - le point de départ : https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html
  - si on joue avec les environnements : https://framagit.org/help/ci/environments
  - Avec Docker : http://containertutorials.com/docker-compose/flask-simple-app.html
  - https://medium.com/the-andela-way/deploying-a-python-flask-app-to-heroku-41250bda27d0
  - https://blog.ruanbekker.com/blog/2019/01/05/tutorial-on-using-gitlab-cicd-pipelines-to-deploy-your-python-flask-restful-api-with-postgres-on-heroku/


