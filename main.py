from flask import Flask,render_template,request
import datetime
import folium
from folium.plugins import LocateControl
from math import cos, log2


app = Flask(__name__)

MY_POSITION = (48.85332, 2.34887)

def zoom(dist, y):
    C = 40075016.686 
    return round(log2(C* cos(y) / dist ) - 8)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/carte')
@app.route('/carte/<int:dist>')
def carte(dist=100):
    start_coords = MY_POSITION
    y = MY_POSITION[0]
    z = zoom(dist, y)
    Carte = folium.Map(location=start_coords, zoom_start=z,
        tiles='https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png',
        attr='<a href="https://cyclosm.org">Cyclosm</a>'
    )
    c = folium.Circle(
    radius = dist*1000,
    location = MY_POSITION,
    color = '#3186cc',
    fill = True,
    fill_color = '#3186cc').add_to(Carte)

    #LocateControl().add_to(folium_map)

    return Carte._repr_html_()

@app.route('/dates',methods=['GET','POST'])
def formulaire():
    date_saisie = '1970-01-01'
    dates = [
            '1000000 seconds',
            '1000000 minutes',
            '100 weeks',
            '1000 days',
            '200 weeks',
            '300 weeks',    
            '2000 days',
            '400 weeks',
            '3000 days',
            '500 weeks',
            '4000 days',
            '100000 hours',
            '600 weeks'
    ]
    if request.method == 'GET':
        return render_template('dates.html', liste_dates = dates,
        dates_affichees = []*len(dates),
        dates = {"saisie": date_saisie} )
    else:
        date_saisie = request.form['date']
        dates_affichées = []
        for datedelta  in dates :
            unite = datedelta.split(' ')[1]
            delta = int(datedelta.split(' ')[0])
            if unite == 'minutes':
                delta = delta * 60
            elif unite == 'hours':
                delta = delta * 60 * 60
            elif unite == 'days':
                delta = delta * 60 * 60 * 24
            elif unite == 'weeks':
                delta = delta * 60 * 60 * 24 * 7
            dates_affichées.append(datetime.datetime.strftime(
            datetime.datetime.fromisoformat(date_saisie) 
            + datetime.timedelta(
                seconds=delta),
            '%d/%m/%Y'
            ))
        return render_template(
            'dates.html',
            liste_dates = dates,
            dates = {"saisie": date_saisie},
            dates_affichees = dates_affichées
            ) 
